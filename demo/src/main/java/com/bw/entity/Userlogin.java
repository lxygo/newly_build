package com.bw.entity;

import java.io.Serializable;

/**
 * 
 * @author auto create
 * @since 1.0,2018-05-15 14:31:26
 */
public class Userlogin implements Serializable {

    private static final long serialVersionUID = 3882816402922541L;

    private Integer id;//

    private String userid;//

    private String username;//

    private String password;//

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
