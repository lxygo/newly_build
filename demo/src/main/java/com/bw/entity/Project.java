package com.bw.entity;

import java.io.Serializable;

/**
 * 
 * @author auto create
 * @since 1.0,2018-05-15 14:31:26
 */
public class Project implements Serializable {

    private static final long serialVersionUID = 7372027178612131L;

    private Integer id;//

    private String projectName;//

    private String theHeir;//

    private String uploadTime;//

    private String itemDescription;//

    private String projectLink;//

    private String sourceUpload;//

    private Integer classificationId;//
    private  String systematicName;

    public String getSystematicName() {
        return systematicName;
    }

    public void setSystematicName(String systematicName) {
        this.systematicName = systematicName;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTheHeir() {
        return this.theHeir;
    }

    public void setTheHeir(String theHeir) {
        this.theHeir = theHeir;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getItemDescription() {
        return this.itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getProjectLink() {
        return this.projectLink;
    }

    public void setProjectLink(String projectLink) {
        this.projectLink = projectLink;
    }

    public String getSourceUpload() {
        return this.sourceUpload;
    }

    public void setSourceUpload(String sourceUpload) {
        this.sourceUpload = sourceUpload;
    }

    public Integer getClassificationId() {
        return this.classificationId;
    }

    public void setClassificationId(Integer classificationId) {
        this.classificationId = classificationId;
    }

}
