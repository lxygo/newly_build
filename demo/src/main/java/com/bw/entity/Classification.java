package com.bw.entity;

import java.io.Serializable;

/**
 * 
 * @author auto create
 * @since 1.0,2018-05-15 14:31:25
 */
public class Classification implements Serializable {

    private static final long serialVersionUID = 4451938731329989L;

    private Integer classificationId;//

    private String systematicName;//

    public Integer getClassificationId() {
        return this.classificationId;
    }

    public void setClassificationId(Integer classificationId) {
        this.classificationId = classificationId;
    }

    public String getSystematicName() {
        return this.systematicName;
    }

    public void setSystematicName(String systematicName) {
        this.systematicName = systematicName;
    }

}
