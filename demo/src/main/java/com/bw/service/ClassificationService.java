package com.bw.service;


import com.bw.entity.Classification;
import java.util.List;

/**
 * 
 * @author auto create
 * @param
 * @since 1.0,2018-05-15 14:31:25
 */
public interface ClassificationService {
    /**
    *@方法名: insertSelective
    *@方法描述: 根据对象插入单条记录
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:25
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer insertSelective(Classification classification) throws Exception;

    /**
    *@方法名: insertBatch
    *@方法描述: 根据对象批量插入记录
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer insertBatch(List<Classification> classification) throws Exception;

    /**
    *@方法名: deleteByPrimaryKey
    *@方法描述: 根据主键删除单条记录
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer deleteByPrimaryKey(Object id);

    /**
    *@方法名: deleteBatchByPrimaryKey
    *@方法描述: 根据主键批量删除
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer deleteBatchByPrimaryKey(List<Object> ids);

    /**
    *@方法名: updateByPrimaryKeySelective
    *@方法描述: 根据主键选择更新单个对象
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer updateByPrimaryKeySelective(Classification classification);

    /**
    *@方法名: findById
    *@方法描述: 根据id 查询对象
    *@param classification
    *@返回值 对象 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Classification findById(Object id);

    /**
    *@方法名: selectByCondition
    *@方法描述: 分条件查询对象
    *@param classification
    *@返回值 List<Classification> 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public List<Classification> selectByCondition(Classification classification);

    /**
    *@方法名: selectCountByCondition
    *@方法描述: 分条件查询对象总数
    *@param classification
    *@返回值 Integer 返回类型
    *@作者：hycx
    *@创建时间 2018年05月15日 14:31:26
    *@修改时间 2018年05月15日 14:31:26
    *@版本 V1.0
    *@异常
    **/
    public Integer selectCountByCondition(Classification classification);


}
