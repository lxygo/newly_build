package com.bw.service.impl;

import com.bw.dao.ClassificationMapper;
import com.bw.entity.Classification;
import com.bw.service.ClassificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "classificationService")
public class ClassificationServiceImpl implements ClassificationService {

    @Autowired
    private ClassificationMapper<Classification> classificationMapper;
    
    @Override
    public Integer insertSelective(Classification classification) throws Exception {
           return this.classificationMapper.insertSelective(classification);
        }

    @Override
    public Integer insertBatch(List<Classification> classification) throws Exception {
           return this.classificationMapper.insertBatch(classification) ;
        }

    @Override
    public Integer deleteByPrimaryKey(Object id) {
            return this.classificationMapper.deleteByPrimaryKey(id);
    }

    public Integer deleteBatchByPrimaryKey(List<Object> ids) {
           return this.classificationMapper.deleteBatchByPrimaryKey(ids);
    }

    @Override
    public Integer updateByPrimaryKeySelective(Classification classification) {
           return this.classificationMapper.updateByPrimaryKeySelective(classification);
    }

    @Override
    public Classification findById(Object id) {
           return (Classification) this.classificationMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Classification> selectByCondition(Classification classification) {
           return (List<Classification>) this.classificationMapper.selectByCondition(classification);
    }

    @Override
    public Integer selectCountByCondition(Classification classification) {
           return  this.classificationMapper.selectCountByCondition(classification);
    }

}
