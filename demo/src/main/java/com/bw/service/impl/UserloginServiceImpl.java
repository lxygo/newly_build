package com.bw.service.impl;

import com.bw.dao.UserloginMapper;
import com.bw.entity.Userlogin;
import com.bw.service.UserloginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "userloginService")
public class UserloginServiceImpl implements UserloginService {

    @Autowired
    private UserloginMapper<Userlogin>userloginMapper;
    
    @Override
    public Integer insertSelective(Userlogin userlogin) throws Exception {
           return this.userloginMapper.insertSelective(userlogin);
        }

    @Override
    public Integer insertBatch(List<Userlogin> userlogin) throws Exception {
           return this.userloginMapper.insertBatch(userlogin) ;
        }

    @Override
    public Integer deleteByPrimaryKey(Object id) {
            return this.userloginMapper.deleteByPrimaryKey(id);
    }

    public Integer deleteBatchByPrimaryKey(List<Object> ids) {
           return this.userloginMapper.deleteBatchByPrimaryKey(ids);
    }

    @Override
    public Integer updateByPrimaryKeySelective(Userlogin userlogin) {
           return this.userloginMapper.updateByPrimaryKeySelective(userlogin);
    }

    @Override
    public Userlogin findById(Object id) {
           return (Userlogin) this.userloginMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Userlogin> selectByCondition(Userlogin userlogin) {
           return (List<Userlogin>) this.userloginMapper.selectByCondition(userlogin);
    }

    @Override
    public Integer selectCountByCondition(Userlogin userlogin) {
           return  this.userloginMapper.selectCountByCondition(userlogin);
    }

}
