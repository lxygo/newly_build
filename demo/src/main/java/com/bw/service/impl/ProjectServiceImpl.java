package com.bw.service.impl;

import com.bw.dao.ProjectMapper;
import com.bw.entity.Project;
import com.bw.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "projectService")
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectMapper<Project> projectMapper;
    
    @Override
    public Integer insertSelective(Project project) throws Exception {
           return this.projectMapper.insertSelective(project);
        }

    @Override
    public Integer insertBatch(List<Project> project) throws Exception {
           return this.projectMapper.insertBatch(project) ;
        }

    @Override
    public Integer deleteByPrimaryKey(Object id) {
            return this.projectMapper.deleteByPrimaryKey(id);
    }

    public Integer deleteBatchByPrimaryKey(List<Object> ids) {
           return this.projectMapper.deleteBatchByPrimaryKey(ids);
    }

    @Override
    public Integer updateByPrimaryKeySelective(Project project) {
           return this.projectMapper.updateByPrimaryKeySelective(project);
    }

    @Override
    public Project findById(Object id) {
           return (Project) this.projectMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Project> selectByCondition(Project project) {
           return (List<Project>) this.projectMapper.selectByCondition(project);
    }

    @Override
    public Integer selectCountByCondition(Project project) {
           return  this.projectMapper.selectCountByCondition(project);
    }

}
