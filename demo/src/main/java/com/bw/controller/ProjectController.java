package com.bw.controller;

import com.bw.entity.Classification;
import com.bw.entity.Project;
import com.bw.service.ClassificationService;
import com.bw.service.ProjectService;
import com.bw.service.UserloginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 作者：赵雄伟
 * 功能:com.bw.controller
 * 时间：14:39 2018/5/15
 */
@Controller
@RequestMapping("/project")
public class ProjectController {
    @Autowired
    private UserloginService service;
    @Autowired
    private ClassificationService service1;
    @Autowired
    private ProjectService service2;
    @RequestMapping("/list")
    public String getProjectList(Model model)
    {

        return "backgroundpage";
    }
    @RequestMapping("backgroundList")
    public String xianshiList(Model model)
    {
        List<Project> list = service2.selectByCondition(new Project());
        model.addAttribute("list",list);
        return "backgroundList";
    }

    @RequestMapping("/login")
    public String login()
    {

        return "login";
    }
    @RequestMapping("/frontpage")
    public String frontpage(Model model)
    {
        List<Classification> classifications = service1.selectByCondition(new Classification());
        model.addAttribute("model",classifications);
        return "addcustomer";
    }
    @RequestMapping("front/desk")
    public String welecome()
    {
        return "welecome";
    }
    @RequestMapping("front/back")
    public String welecomeback()
    {
        return "background_page";
    }
    @RequestMapping("systematicName")
    public  String systematicName(Project project,Model model)
    {
        List<Project> list = service2.selectByCondition(project);
        model.addAttribute("list",list);
        return "systematicName";
    }
    @RequestMapping("xiangqing")
    @ResponseBody
    public  Project xiangqing(Integer id)
    {
        Project project = service2.findById(id);
        return project;
    }
    @RequestMapping("frontpages")
    public  String frontPage(Model model)
    {
        List<Classification> classifications = service1.selectByCondition(new Classification());
        model.addAttribute("model",classifications);
        return "frontpage";
    }



}
