<%--
  Created by IntelliJ IDEA.
  User: zhaox
  Date: 2018/5/16
  Time: 8:30
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>展示列表</title>

    <link rel="stylesheet" href="assets\css\bootstrap.css" type="text/css"></link>
    <script type="text/javascript" src="assets\js\jquery.js"></script>
    <script type="text/javascript" src="assets\js\bootstrap.min.js"></script>
    <script  type="text/javascript">


    </script>
</head>
<body>
<div >
    <table class="container table table-bordered table table-striped">
        <tr>
            <td>项目id</td>
            <td>项目名称</td>
            <td>上传人</td>
            <td>项目描述</td>
            <td>项目链接</td>
            <td>源码上传</td>
            <td>项目图片</td>
            <td>项目分类</td>
        </tr>
        <c:forEach items="${list}" var="s">
            <tr>
                <td>${s.id}</td>
                <td>${s.projectName}</td>
                <td>${s.id}</td>
                <td>${s.theHeir}</td>
                <td>${s.itemDescription}</td>
                <td>${s.projectLink}</td>
                <td>${s.sourceUpload}</td>
                <td>${s.systematicName}</td>
            </tr>
        </c:forEach>

    </table>

</div>


</body>
</html>
