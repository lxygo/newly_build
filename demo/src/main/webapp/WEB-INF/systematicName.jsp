<%--
  Created by IntelliJ IDEA.
  User: zhaox
  Date: 2018/5/16
  Time: 8:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>展示列表</title>

    <link rel="stylesheet" href="assets\css\bootstrap.css" type="text/css"></link>
    <script type="text/javascript" src="assets\js\jquery.js"></script>
    <script type="text/javascript" src="assets\js\bootstrap.min.js"></script>
    <script  type="text/javascript">
        function xiangqing(id) {
            $.post(
                "xiangqing",
                {"id":id},
                function (obj) {
                    $("#inputEmail3").attr("value",obj.projectName);
                    $("#inputEmail").attr("value",obj.theHeir);
                    $("#inputEmail2").attr("value",obj.uploadTime);
                    $("#projectLink").html(obj.itemDescription);
                },
                "json"

            );
        }


    </script>
</head>
<body>
        <div class="col-md-12" style="margin-top: 20px;">
            <c:forEach items="${list}" var="s">
                <div class="col-md-4">
                    <img alt="图片预览" src="img\1b90e4dc219f41dd70f7b9c14fcb4f0a.jpg" id="img" class="img-responsive" style="margin-bottom: 10px;">
                    上传人:${s.theHeir}<br>
                    上传时间:${s.uploadTime}<br>
                    <button type="button" class="btn btn-info" style="margin-left: 10px; margin-top: 10px;">下载</button>
                    <button  type="button" class="btn btn-info" style="margin-left: 10px; margin-top: 10px;" data-toggle="modal" data-target=".myModal2" onclick="xiangqing(${s.id})">查看详情</button>
                </div>

            </c:forEach>

        </div>
        <div class="modal myModal2"><!---->
            <div class="modal-dialog modal-lg"><!--还有modal-sm，modal-md-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">×</button>
                        <h4><font color="#9932cc">查看详情页面</font></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group col-md-8">
                            <label for="inputEmail3" class="col-sm-2 control-label">项目名称:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail3" placeholder="项目名称"  readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="inputEmail3" class="col-sm-2 control-label">上传人:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail" placeholder="上传人" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="inputEmail3" class="col-sm-2 control-label">上传时间:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputEmail2" placeholder="上传时间" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="inputEmail3" class="col-sm-2 control-label">项目描述:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3"  placeholder="项目描述" id="projectLink" readonly="readonly"></textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="inputEmail3" class="col-sm-2 control-label">项目链接:</label>
                            <div class="col-sm-10">
                               <a href="https://www.baidu.com">项目链接111</a>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-primary" data-dismiss="modal">确定</button>
                    </div>
                </div>
            </div>
        </div>

</body>
</html>
