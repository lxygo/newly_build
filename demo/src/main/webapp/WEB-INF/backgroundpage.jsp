<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dream</title>
    <!-- Bootstrap Styles-->
    <link href="<%request.getContextPath(); %>/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="<%request.getContextPath(); %>/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="<%request.getContextPath(); %>/assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <!-- 表单验证CSS -->
    <link href="<%request.getContextPath(); %>/assets/css/bootstrapValidator.css" rel="stylesheet" />
    <!-- 左右移动多选框插件~Multiselect--CSS -->
    <link href="<%request.getContextPath(); %>/assets/css/style.css" rel="stylesheet" />
    <!-- 上传-CSS -->
    <link href="<%request.getContextPath(); %>/assets/css/fileinput.min.css" rel="stylesheet" />


</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">大数据项目库</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/customer_list">大数据项目库</a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Doe</strong>
                                <span class="pull-right text-muted">
                                        <em>Today</em>
                                    </span>
                            </div>
                            <div>Lorem Ipsum has been the industry's standard dummy text ever since the 1500sassets.</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem Ipsum has been the industry's standard dummy text ever since an kwilnwassets.</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <strong>John Smith</strong>
                                <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                            </div>
                            <div>Lorem Ipsum has been the industry's standard dummy text ever since theassets.</div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>Read All Messages</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-tasks">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <span class="pull-right text-muted">28% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100" style="width: 28%">
                                        <span class="sr-only">28% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <span class="pull-right text-muted">85% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
                                        <span class="sr-only">85% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> New Comment
                                <span class="pull-right text-muted small">4 min</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                <span class="pull-right text-muted small">12 min</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> Message Sent
                                <span class="pull-right text-muted small">4 min</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-tasks fa-fw"></i> New Task
                                <span class="pull-right text-muted small">4 min</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                <span class="pull-right text-muted small">4 min</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-alerts -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="javascript:void (0)" onclick="zhanshi('/project/backgroundList')" ><i class="fa fa-table"></i>项目列表</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <iframe style='width: 100%;height:1000px;' id="iframe"  src="front/back">

        </iframe>
        <footer><p>Copyright © 2016.Company name All rights reserved - <a href="http://www.zhenmeishuma.com/" target="_blank" title="臻美数码">臻美数码</a> - Collect from <a href=" " title="样片展示" target="_blank">样片展示</a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script type="text/javascript">
    function zhanshi(url) {

         $("#iframe").prop("src",url);
    }
</script>
<script src="<%request.getContextPath(); %>/assets/js/jquery.js"></script>
<!-- Bootstrap Js -->
<script src="<%request.getContextPath(); %>/assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="<%request.getContextPath(); %>/assets/js/jquery.metisMenu.js"></script>
<!-- 表单验证CSS -->
<script src="<%request.getContextPath(); %>/assets/js/bootstrapValidator.js"></script>
<!-- 左右移动多选框插件~Multiselect -->
<script src="<%request.getContextPath(); %>/assets/js/multiselect.js"></script>
<!-- 上传 -->
<script src="<%request.getContextPath(); %>/assets/js/fileinput.min.js"></script>
<script src="<%request.getContextPath(); %>/assets/js/locales/zh.js"></script>



<script>
    $(document).ready(function (){
        $("#form").bootstrapValidator({
            live: 'enabled',//验证时机，enabled是内容有变化就验证（默认），disabled和submitted是提交再验证
            excluded: [':disabled', ':hidden', ':not(:visible)'],//排除无需验证的控件，比如被禁用的或者被隐藏的
            submitButtons: '#btn-test',//指定提交按钮，如果验证失败则变成disabled，但我没试成功，反而加了这句话非submit按钮也会提交到action指定页面
            message: '通用的验证失败消息',//好像从来没出现过
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                customer_number: {
                    validators: {
                        notEmpty: {//检测非空,radio也可用
                            message: '订单编号不能为空'
                        },
                        regexp: {//正则验证
                            regexp: /^[a-zA-Z0-9_+-—.]{3,20}$/,
                            message: '所输入的字符不符要求'
                        },
                    }
                },
                customer_name: {
                    validators: {
                        notEmpty: {//检测非空,radio也可用
                            message: '客户姓名不能为空'
                        },
                        regexp: {//正则验证
                            regexp: /^[\u4E00-\u9FA5a-zA-Z0-9_]{3,20}$/,
                            message: '所输入的字符不符要求'
                        },

                    }
                },
                phone_number: {
                    validators: {
                        notEmpty: {//检测非空,radio也可用
                            message: '联系方式不能为空'
                        },
                        regexp: {//正则验证
                            regexp: /^1[3|4|5|8][0-9]\d{8}$/,
                            message: '所输入的字符不符要求'
                        },

                    }
                },
                unit_price: {
                    validators: {
                        notEmpty: {//检测非空,radio也可用
                            message: '订单价格不能为空'
                        },
                        regexp: {//正则验证
                            regexp: /^[0-9]+$/,
                            message: '所输入的字符不符要求'
                        },
                    }
                }
            }
        });
        <!--左后横移事件-->
        $("#submit").click(function () {//非submit按钮点击后进行验证，如果是submit则无需此句直接验证
            $("#form-test").bootstrapValidator('validate');//提交验证
            if ($("#form-test").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                alert("yes");//验证成功后的操作，如ajax
            }
        });
        $("#reset").click(function () {//刷新界面
            location.href="/to_add_customer";
        });
        $('.js-multiselect').multiselect({
            right: '#js_multiselect_to_1',
            rightAll: '#js_right_All_1',
            leftAll: '#js_left_All_1'
        });
        <!--上传文件-->
        $("#fileInput").fileinput('destroy');
        $("#fileInput").fileinput({
            language : 'zh', //设置语言
            uploadUrl:"save_pic", //后台Controller
            uploadAsync: false,//leinput默认是异步上传的，即uploadAsync: true，你会发现如果你上传多张图片，请求就会发送多次，每次只提交一张图片，如果想一次提交所有图片，则需要将uploadAsync设置为false。
            showCaption: false,//是否显示标题
            minFileCount: 1,//表示允许同时上传的最少文件个数
            maxFileCount: 6, //表示允许同时上传的最大文件个数
            msgFilesTooMany:"选择上传的文件数量{n} 超过允许的最大数值{m}!",//当超出最大上传数弹出提示
            allowedFileExtensions: ['jpg','png'],
            fileActionSettings:{showUpload: false},// 控制图片预览中的上传按钮是否显示
            initialPreviewAsData: true // 识别是否只发送预览数据而不是标记p
        }).on('filebatchpreupload', function(event, data) {
            if($('.imgClass').length>=6){
                var img = $(".kv-preview-thumb");
                img[3].remove();
                $(".kv-upload-progress").addClass("hide");
                return {
                    message: "最多只能上传6张!"
                };
            }
        });
        /*接受返回的文件路径*/
        $('#fileInput').on('filebatchuploadsuccess', function(event, data, previewId, index) {
            var response = data.response;
            $.each(response,function(id,path){//上传完毕，将文件名返回
                $("#form").append("<input class='imgClass' name='filePath' type='hidden' value='"+path.pathUrl+"'>");
            });
        });
    });
</script>
<!-- Custom Js -->
<script src="<%request.getContextPath(); %>/assets/js/custom-scripts.js"></script>


<script>

</script>
</body>
</html>