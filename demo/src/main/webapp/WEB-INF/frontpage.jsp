<html >
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>Home</title>
    <meta name="description" content="">
    <meta name="author" content="WebThemez">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script type="text/javascript" src="http://explorercanvas.googlecode.com/svn/trunk/excanvas.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/isotope.css" media="screen" />
    <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/da-slider.css" />
    <link rel="stylesheet" href="css/styles.css"/>
    <!-- Font Awesome -->
    <link href="font/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets\css\bootstrap.css" type="text/css"></link>
    <script type="text/javascript" src="assets\js\jquery.js"></script>
    <script type="text/javascript" src="assets\js\bootstrap.min.js"></script>
    <script  type="text/javascript">
        function chaxunyemian() {

        }
        function xiangqing(id) {
            $.post(
                "xiangqing",
                {"id":id},
                function (obj) {
                    $("#inputEmail3").attr("value",obj.projectName);
                    $("#inputEmail").attr("value",obj.theHeir);
                    $("#inputEmail2").attr("value",obj.uploadTime);
                    $("#projectLink").html(obj.itemDescription);
                },
                "json"

            );
        }


    </script>
</head>

<body>
<header class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav" id="mainNav">
                    <li class="active"><a href="#home" class="scroll-link" >欢迎页面</a></li>
                    <c:forEach items="${model}" var="s">

                        <li><a  class="scroll-link"
                                <c:if test="${s.systematicName=='金融'}">href="#aboutUs"
                                </c:if>
                                <c:if test="${s.systematicName=='电商'}">href="#services"
                                </c:if>
                                <c:if test="${s.systematicName=='租车'}">href="#portfolio"
                                 </c:if>
                                <c:if test="${s.systematicName=='物流'}">href="#team"
                                </c:if>
                         onclick="chaxunyemian(${s.classificationId})">${s.systematicName}</a></li>

                    </c:forEach>
                    <li><a href="#contactUs" class="scroll-link">Contact Us</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</header>
<!--/.header-->
<div id="#top"></div>
<section id="home" class="page-section">
    <div class="banner-container">
        <div class="container banner-content">
            <<!--h1>Responsive Page Scrolling<br/><span>with Bootstrap</span></h1>
		    <p class="lead">Use the navigation above to scroll to different page sections.</p>
		    <p><a href="#about" class="button-outline"> More.. </a></p>-->
            <div id="da-slider" class="da-slider">
                <div class="da-slide">
                    <h2>努力学习，实现梦想</h2>
                    <p>So I must study hard, to achieve the dream</p>
                    <a href="#" class="da-link button-outline">奋斗吧！少年</a>
                    <div class="da-img"></div>
                </div>
                <div class="da-slide">
                    <h2>学习使我快乐</h2>
                    <p>Learning makes me happy.</p>
                    <a href="#" class="da-link button-outline">努力加油吧！</a>
                    <div class="da-img"></div>
                </div>
                <div class="da-slide">
                    <h2>朝着我们成功的路上进发</h2>
                    <p>On our way to success.</p>
                    <a href="#" class="da-link button-outline">奋斗吧！少年</a>
                    <div class="da-img"></div>
                </div>
                <div class="da-slide">
                    <h2>愿我们可以高薪就业</h2>
                    <p>May we obtain high salary employment</p>
                    <a href="#" class="da-link button-outline">努力加油吧！</a>
                    <div class="da-img"></div>
                </div>
                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>
            </div>
        </div>
    </div>
    <!--/.container-->
</section>

<section id="aboutUs" class="page-section pDark pdingBtm30">
    <div class="container">
        <div class="heading text-center">
            <!-- Heading -->
            <h2>关于金融的项目</h2>
            <p>知识不是某种完备无缺、纯净无瑕、僵化不变的东西。它永远在创新，永远在前进。——普良尼施尼柯夫<br>Knowledge is not something complete, immaculate, or rigid.It is always innovating, always moving forward.</p>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="tray text-center">
                    <div><img src="img\1b90e4dc219f41dd70f7b9c14fcb4f0a.jpg" alt="对不起显示图片错误" class="img-responsive"></div>
                    <h3>淘淘商城</h3>
                    <!-- Paragraph -->
                    <p>上传人:张三<br>
                        上传时间:2018-5-17<br>
                        <button type="button" class="btn btn-info" style="margin-left: 10px; margin-top: 10px;">下载</button>
                        <button  type="button" class="btn btn-info" style="margin-left: 10px; margin-top: 10px;" data-toggle="modal" data-target=".myModal2" onclick="xiangqing(${s.id})">查看详情</button>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="tray text-center">
                    <!-- Heading -->
                    <h3><i class="fa fa-cloud color"></i>极车公社</h3>
                    <!-- Paragraph -->
                    <p>Duis aute irure dolor in repreh enderit in voluptate velit esse cillum dolore pariatur.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="tray text-center">
                    <!-- Heading -->
                    <h3><i class="fa fa-home color"></i>微信小程序</h3>
                    <!-- Paragraph -->
                    <p>Duis aute irure dolor in repreh enderit in voluptate velit esse cillum dolore pariatur.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="tray text-center">
                    <!-- Heading -->
                    <h3><i class="fa fa-coffee color"></i> &nbsp;Responsive</h3>
                    <!-- Paragraph -->
                    <p>Duis aute irure dolor in repreh enderit in voluptate velit esse cillum dolore pariatur.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal myModal2"><!---->
        <div class="modal-dialog modal-lg"><!--还有modal-sm，modal-md-->
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h4><font color="#9932cc">查看详情页面</font></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-md-8">
                        <label for="inputEmail3" class="col-sm-2 control-label">项目名称:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="项目名称"  readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputEmail3" class="col-sm-2 control-label">上传人:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEmail" placeholder="上传人" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputEmail3" class="col-sm-2 control-label">上传时间:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEmail2" placeholder="上传时间" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputEmail3" class="col-sm-2 control-label">项目描述:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3"  placeholder="项目描述" id="projectLink" readonly="readonly"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputEmail3" class="col-sm-2 control-label">项目链接:</label>
                        <div class="col-sm-10">
                            <a href="https://www.baidu.com">项目链接111</a>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal">确定</button>
                </div>
            </div>
        </div>
    </div>
    <!--/.container-->
</section>


<section id="services" class="page-section darkBg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>Our Services</h2>
                    <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">E-Commerce</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Web Development</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Mobile Dev</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
        </div><!--/.row-->
        <div class="gap"></div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Web Marketing</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Graphic Design</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
            <div class="col-md-4 col-sm-6">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-cloud color fa-5x"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">SEO Services</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore oluptate velit es pariatur</p>
                    </div>
                </div>
            </div><!--/.col-md-4-->
        </div><!--/.row-->
    </div><!--/.container-->
</section>


<section id="portfolio" class="page-section section appear clearfix">
    <div class="container">

        <div class="heading text-center">
            <!-- Heading -->
            <h2>Our Works</h2>
            <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>
        </div>

        <div class="row">
            <nav id="filter" class="col-md-12 text-center">
                <ul>
                    <li><a href="#" class="current btn-theme btn-small" data-filter="*">All</a></li>
                    <li><a href="#"  class="btn-theme btn-small" data-filter=".webdesign" >Web Design</a></li>
                    <li><a href="#"  class="btn-theme btn-small" data-filter=".photography">Photography</a></li>
                    <li ><a href="#" class="btn-theme btn-small" data-filter=".print">Print</a></li>
                </ul>
            </nav>
            <div class="col-md-12">
                <div class="row">
                    <div class="portfolio-items isotopeWrapper clearfix" id="3">

                        <article class="col-md-4 col-sm-4 isotopeItem webdesign">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img1.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img1.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem photography">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img2.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img2.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>


                        <article class="col-md-4 col-sm-4 isotopeItem photography">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img3.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img3.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem print">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img4.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img4.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem photography">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img5.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img5.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem webdesign">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img6.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img6.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem print">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img7.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img7.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem photography">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img8.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img8.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="col-md-4 col-sm-4 isotopeItem print">
                            <div class="portfolio-item">
                                <img src="images/portfolio/img9.jpg" alt="" />
                                <div class="portfolio-desc align-center">
                                    <div class="folio-info">
                                        <a href="images/portfolio/img9.jpg" class="fancybox"><h5>Project Name</h5>
                                            <i class="fa fa-expand fa-2x"></i></a>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>


            </div>
        </div>

    </div>
</section>

<section id="team" class="page-section team-member">
    <div class="container">
        <div class="heading text-center">
            <!-- Heading -->
            <h2>Meet The Team</h2>
            <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>
        </div>
        <!-- Team Member's Details -->
        <div id="meet-the-team" class="row">
            <div class="col-md-3 col-xs-6">
                <div class="center text-center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="images/photo-1.jpg" alt=""></p>
                    <h5>Jonnti Renlly<small class="designation muted">Senior Vice President</small></h5>
                    <p>lorem Ipsum available, but the majority have suffered alteration.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>

            <div class="col-md-3 col-xs-6">
                <div class="center text-center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="images/photo-1.jpg" alt=""></p>
                    <h5>Jonnti Renlly<small class="designation muted">Senior Vice President</small></h5>
                    <p>lorem Ipsum available, but the majority have suffered alteration.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="center text-center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="images/photo-1.jpg" alt=""></p>
                    <h5>Jonnti Renlly<small class="designation muted">Senior Vice President</small></h5>
                    <p>lorem Ipsum available, but the majority have suffered alteration.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="center text-center">
                    <p><img class="img-responsive img-thumbnail img-circle" src="images/photo-1.jpg" alt=""></p>
                    <h5>Jonnti Renlly<small class="designation muted">Senior Vice President</small></h5>
                    <p>lorem Ipsum available, but the majority have suffered alteration.</p>
                    <a class="btn btn-social btn-facebook" href="#"><i class="icon-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="icon-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="icon-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="icon-linkedin"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="heading text-center">
                    <h2>What our clients say</h2>
                    <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>
                </div>
                <div class="gap"></div>
                <div class="row">
                    <div class="col-md-6">
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit majority have suffered alteration</p>
                            <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                        </blockquote>
                    </div>
                    <div class="col-md-6">
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit majority have suffered alteration</p>
                            <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
</section>
<footer class="page-section darkBg" id="contactUs">
    <div class="container">

        <div class="row">
            <div class="heading text-center">
                <!-- Heading -->
                <h2>Contact Us</h2>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered.</p>
            </div>
        </div>

        <div class="row mrgn30">
            <div class="col-sm-3">
                <h4>Address:</h4>
                <address>
                    WebThemez Company<br>
                    134 Stilla. Tae., 414515<br>
                    Leorislon, SA 02434-34534 USA <br>
                </address>
                <h4>Contact Info:</h4>
                <ul>
                    <li><i class="fa fa-phone"></i>1-123-345-6789</li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i> contact@website.com</a></li>
                    <li><i class="fa fa-flag"></i>123 Smith Drive, Baltimore, MD 21212</li>
                </ul>
            </div>


            <div class="col-sm-4">
                <!--NOTE: Update your email Id in "contact_me.php" file in order to receive emails from your contact form-->
                <form name="sentMessage" id="contactForm"  novalidate>
                    <h3>Contact Form</h3>
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" class="form-control"
                                   placeholder="Full Name" id="name" required
                                   data-validation-required-message="Please enter your name" />
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input type="email" class="form-control" placeholder="Email"
                                   id="email" required
                                   data-validation-required-message="Please enter your email" />
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
		<textarea rows="10" cols="100" class="form-control"
                  placeholder="Message" id="message" required
                  data-validation-required-message="Please enter your message" minlength="5"
                  data-validation-minlength-message="Min 5 characters"
                  maxlength="999" style="resize:none"></textarea>
                        </div>
                    </div>
                    <div id="success"> </div> <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary pull-right">Send</button><br />
                </form>
            </div>
        </div>
    </div>
    <!--/.container-->
</footer>
<!--/.page-section-->
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="socialIcons">
                    <li><a href="#" class="fbIcon" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a></li>
                    <li><a href="#" class="twitterIcon" target="_blank"><i class="fa fa-twitter-square fa-lg"></i></a></li>
                    <li><a href="#" class="googleIcon" target="_blank"><i class="fa fa-google-plus-square fa-lg"></i></a></li>
                    <li><a href="#" class="pinterest" target="_blank"><i class="fa fa-pinterest-square fa-lg"></i></a></li>
                </ul>   <div class="pull-right webThemez">Copyright &copy; 2016.Company name All rights reserved.More Templates <a href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a> - Collect from <a href="http://www.cssmoban.com/" title="网页模板" target="_blank">网页模板</a></div>
            </div>
        </div>  <!-- / .row -->
    </div>
</section>
<a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a>

<!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]-->
<script src="js/modernizr-latest.js"></script>
<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="js/jquery.nav.js" type="text/javascript"></script>
<script src="js/jquery.cslider.js" type="text/javascript"></script>
<script src="contact/jqBootstrapValidation.js"></script>
<script src="contact/contact_me.js"></script>
<script src="js/custom.js" type="text/javascript"></script>
</body>
</html>